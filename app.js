var express = require('express');
var cookieSession = require('cookie-session')
var bodyParser = require('body-parser')
var serveStatic = require('serve-static')


var app = express();

app.use(express.static(__dirname+'/static'))
var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(cookieSession({
  name: 'session',
  keys: ['key1', 'key2'],
 
  maxAge: 24 * 60 * 60 * 1000 //24hours.
})).use(function(req, res, next){
    if (typeof(req.session.list) == 'undefined') {
        req.session.list = [];
    }
    next();
});

app.get('/', function(req, res) {
    res.render('index.ejs',{id :null});
});

app.post('/',urlencodedParser, function(req, res) {
	if(!req.session.id){
		req.session.id = req.body.id;
	}
    res.render('index.ejs',{id :req.session.id});
});

app.get('/loggin', function(req, res) {
    res.render('loggin.ejs');
});

app.get('/signup', function(req, res) {
    res.render('signup.ejs');
});

app.listen(8080);